/**
 * Created by james on 5/9/2017.
 */

var xhr = new XMLHttpRequest();

xhr.onreadystatechange = function(){
    if(this.readyState === 4 && this.status === 200){
        var jsonArr = JSON.parse(this.responseText);

        //update the index.html news section
        var ele = document.getElementById("#featureHead");

        if(ele) {
            var newsSectionHead = document.querySelector("#featureHead");

            newsSectionHead.innerHTML = '<strong>' + jsonArr.news[5].title + '</strong>';
        }

        ele = document.getElementById("featureDate");

        if(ele) {
            var featureNewDate = document.querySelector("#featureDate");

            featureNewDate.innerHTML = jsonArr.news[5].postDate;
        }

        ele = document.getElementById("indexNews");
        {
            var indexNewsSection = document.querySelectorAll("#indexNews article");

            for (i = 0; i < indexNewsSection.length; i++) {
                indexNewsSection[i].getElementsByTagName('p')[0].innerHTML = jsonArr.news[i].postDate;
                indexNewsSection[i].getElementsByTagName('h4')[0].innerHTML = jsonArr.news[i].title;

            }
        }

        //update index.html live events section
        ele= document.getElementById("mainTour");

        if(ele) {
            var indexTableLength = document.getElementById("mainTour").rows.length;

            for (i = 0; i < indexTableLength; i++) {
                var indexTour = document.getElementById("mainTour").rows[i].cells;

                for (y = 0; y < indexTour.length; y++) {
                    indexTour[0].innerHTML = jsonArr.events[i].date;
                    indexTour[1].innerHTML = '<h3>'+jsonArr.events[i].city + ' ,' + jsonArr.events[i].state + '<strong>' + jsonArr.events[i].venue + '</strong></h3>';
                    indexTour[2].innerHTML = '<a href="' + jsonArr.events[i].locationURL + '" target="_blank">' +
                        '<img src="images/location.png" alt="map location"></a> ' +
                        '<a href="' + jsonArr.events[i].ticketsURL + '" target="_blank"><img src="images/ticket.png" alt="ticket"></a></td>'
                }
            }
        }

        //update the about the band section on index.html
        ele = document.getElementById("quote");

        if(ele) {
            var aboutQuote = document.querySelector("#quote");

            aboutQuote.innerHTML = '<em> &quot' + jsonArr.about.quote + '&quot</em>';
        }

        ele = document.getElementById("info");

        if(ele) {
            var aboutCopy = document.querySelector("#info");

            aboutCopy.innerHTML = jsonArr.about.copy;
        }

        ele = document.getElementById("members");

        if(ele) {
            var aboutImages = document.querySelectorAll("#members img");

            for (i = 0; i < aboutImages.length; i++) {
                aboutImages[i].src = jsonArr.members[i].imageURL;
            }
            //console.log(aboutImages);
        }

        ele = document.getElementById("featureNews");

        if(ele) {
            var featNewsSection = document.querySelectorAll("#featureNews article");

            for (i = 0; i < featNewsSection.length; i++) {
                featNewsSection[i].getElementsByTagName('h3')[0].innerHTML = jsonArr.news[i].title + '<strong>' + jsonArr.news[i].postDate + '</strong>';
                featNewsSection[i].getElementsByTagName('img')[0].src = jsonArr.news[i].imageURL;
                featNewsSection[i].getElementsByTagName('p')[0].innerHTML = jsonArr.news[i].text;
            }
        }

        ele = document.getElementById("news");

        if(ele) {
            var newsSection = document.querySelectorAll("#news article");

            for (i = 0; i < newsSection.length; i++) {
                newsSection[i].getElementsByTagName('h3')[0].innerHTML = jsonArr.news[i].title + '<strong>' + jsonArr.news[i].postDate + '</strong>';
                newsSection[i].getElementsByTagName('img')[0].src = jsonArr.news[i].imageURL;
                newsSection[i].getElementsByTagName('p')[0].innerHTML = jsonArr.news[i].text;
            }
            console.log(jsonArr.news.length);
            //var section = document.querySelector("#news");
            //var data = '<article>';
            //for (i=4;i<jsonArr.news.length;i++){
              //  data += '<h3>'+jsonArr.news[i].title+'<strong>'+jsonArr.news[i].postDate +'</strong></h3>';
                //data += '<img src="'+jsonArr.news[i].imageURL+'" alt="Image" style="width:300px;height:200px">';
                //data += '<p>'+jsonArr.news[i].text+'</p>';
                //data += '</article>';
           // }

            //section.insertAdjacentHTML('beforeEnd', data);
        }

        ele= document.getElementById("featTour");

        if(ele) {
            var featTableLength = document.getElementById("featTour").rows.length;

            for (i = 0; i < featTableLength; i++) {
                var featIndexTour = document.getElementById("featTour").rows[i].cells;

                for (y = 0; y < featIndexTour.length; y++) {
                    featIndexTour[0].innerHTML = jsonArr.events[3].date;
                    featIndexTour[1].innerHTML = '<h3>' + jsonArr.events[3].city + ' ,' + jsonArr.events[3].state + '<strong>' + jsonArr.events[3].venue+'</strong></h3>';
                    featIndexTour[2].innerHTML = '<a href="' + jsonArr.events[3].locationURL + '" target="_blank">' +
                        '<img src="images/location.png" alt="map location"></a> ' +
                        '<a href="' + jsonArr.events[3].ticketsURL + '" target="_blank"><img src="images/ticket.png" alt="ticket"></a></td>'
                }
            }
        }

        ele= document.getElementById("tourInfo");

        if(ele) {
            var tableLength = document.getElementById("tourInfo").rows.length;

            for (i = 0; i < tableLength; i++) {
                var tour = document.getElementById("tourInfo").rows[i].cells;

                for (y = 1; y < tour.length; y++) {
                    tour[0].innerHTML = jsonArr.events[i].date;
                    tour[1].innerHTML = '<h3>'+jsonArr.events[i].city + ' ,' + jsonArr.events[i].state + '<strong>' + jsonArr.events[i].venue + '</strong></h3>';
                    tour[2].innerHTML = '<a href="' + jsonArr.events[i].locationURL + '" target="_blank">' +
                        '<img src="images/location.png" alt="map location"></a> ' +
                        '<a href="' + jsonArr.events[i].ticketsURL + '" target="_blank"><img src="images/ticket.png" alt="ticket"></a></td>'
                }
            }
        }

        ele = document.getElementById("memberInfo");

        if(ele){
            var memberProfile = document.querySelectorAll('#memberInfo article');

            console.log(memberProfile);
            for(i=0;i<memberProfile.length;i++){
                memberProfile[i].getElementsByTagName('h3')[0].innerHTML = jsonArr.members[i].instrument + '<strong>' + jsonArr.members[i].firstname+' '+jsonArr.members[i].lastname + '</strong>';
                memberProfile[i].getElementsByTagName('img')[0].src = jsonArr.members[i].imageURL;
            }
        }

    }
};

xhr.open('GET','data/data.json', true);
xhr.send(null);