/**
 * Created by james on 5/23/2017.
 */

//global variables since they are used more than once in 2 different functions
var elNameMsg = document.querySelector("#nameFeedback");
var elEmailMsg = document.querySelector("#emailFeedback");
var elMesFeedback = document.querySelector('#messFeedback');

function setup(){


    //create the function that will check on the name input
    function checkName(e){

        var el = e.target;

        //checks if the user entered more than 2 characters and isn't blank space
        if(el.value.length < 2 || el.value === ""){

            //displays a message of what is wrong
            elNameMsg.innerHTML = "Please enter your name.";

            //doesn't allow the user to move on to the next form element
            document.getElementById('name').focus();
        }else{
            elNameMsg.innerHTML = "";
            //document.getElementById('email').focus();
        }
    }

    var elName = document.querySelector('#name');

    //waits for the user to move onto another area of the form and checks what the user has input
    elName.addEventListener('blur', checkName, false);

    var emailFilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    function checkEmail(e){

        var el = e.target;

        console.log(el.value);
        console.log(emailFilter.test(el.value));

        if(el.value === null || el.value === "" || emailFilter.test(el.value)=== false){
            elEmailMsg.innerHTML = "Please enter a valid email address";

            document.getElementById('email').focus();
        }else{
            elEmailMsg.innerHTML = "";
            //document.getElementById('message').focus();
        }
    }
    var elEmail = document.querySelector('#email');

    elEmail.addEventListener('blur',checkEmail, false);

    function checkMessage(e){

        var el = e.target;

        if(el.value === null || el.value.length < 5 || el.value === ""){
            elMesFeedback.innerHTML = "Please fill out this area out.";
            document.getElementById('message').focus();
        }else{
            elMesFeedback.innerHtml = "";
        }
    }

    var elMessage = document.querySelector('#message');

    elMessage.addEventListener('blur', checkMessage, false);

    var elForm = document.querySelector('#form');

    function checkForm(event){

        var submitError = document.getElementById('submitFeedback');

        if(elName.value===null || elName.value === ""){
            submitError.innerHTML = "Please fill out all areas before submitting.";
            document.getElementById('name').focus();
            event.preventDefault();
        }else if(elEmail.value === null || elEmail.value === ""){

            submitError.innerHTML = "Please fill out all areas before submitting.";
            document.getElementById('email').focus();
            event.preventDefault();
        }else if(elMessage.value === null || elMessage.value === ""){

            submitError.innerHTML = "Please fill out all areas before submitting.";
            document.getElementById('message').focus();
            event.preventDefault();
        }
    }


    elForm.addEventListener('submit', checkForm, false);

}

window.addEventListener('load', setup, false);